package com.lupawaktu.halalindonesia;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Mind on 6/9/2017.
 */
public class Produk extends AppCompatActivity implements Proses.ViewData {
    @BindView(R.id.cari)
    Button cari;
    @BindView(R.id.etcari)
    EditText etcari;
    @BindView(R.id.status)
    TextView status;
    @BindView(R.id.list)
    ListView listView;
    @BindView(R.id.loading)
    ProgressBar loading;

    Proses proses;
    BaseAdapter adapter;
    ArrayList<HashMap<String,String>> data;
    String this_page, next_page, statusdata;
    String _cari;
    int prev;

    String menu;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.produk);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        data = new ArrayList<>();
        proses = new Proses(this);
        switch (getIntent().getStringExtra("menu")){
            case "produk":
                getSupportActionBar().setTitle("Nama Produk");
                menu = getString(R.string.produk);
                etcari.setInputType(InputType.TYPE_CLASS_TEXT);
                etcari.setHint("Cari Nama Produk ..");
                break;
            case "produsen" :
                getSupportActionBar().setTitle("Nama Produsen");
                menu = getString(R.string.produsen);
                etcari.setInputType(InputType.TYPE_CLASS_TEXT);
                etcari.setHint("Cari Nama Produsen ..");
                break;
            case "sertifikat" :
                getSupportActionBar().setTitle("Nomor Sertifikat");
                menu = getString(R.string.sertifikat);
                etcari.setInputType(InputType.TYPE_CLASS_NUMBER);
                etcari.setHint("Cari Nomor Sertifikat ..");
                break;
        }
        status.setVisibility(View.GONE);
        loading.setVisibility(View.GONE);
        listView.addFooterView(View.inflate(this, R.layout.loading, null));
        cari.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                data.clear();
                _cari = etcari.getText().toString();
                if ( !TextUtils.isEmpty(_cari) ){
                    loading.setVisibility(View.VISIBLE);
                    status.setVisibility(View.GONE);
                    proses.getDataFromServer(_cari, menu, 0);
                } else {
                    Toast.makeText(getApplicationContext(), "Anda belum memasukan keyword.", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    public void success(ResponseModel model) {
        loading.setVisibility(View.GONE);
        try {
            JSONObject object = new JSONObject(model.getResponse());
            statusdata = object.getString("status");
            if (object.getString("status").equals("success")){
                JSONArray jsonArray = object.getJSONArray("data");
                this_page = object.getString("this_page");
                next_page = object.getString("next_page");
                proses.showConvertData(jsonArray, data);
            } else {
                status.setVisibility(View.VISIBLE);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void failure(ResponseModel model) {
        loading.setVisibility(View.GONE);
    }

    @Override
    public void tampilData(ModelDataArray modelDataArray) {
        if(modelDataArray.getDataModels().size() != 0) {
            if (adapter != null) {
                adapter.notifyDataSetChanged();
                listView.setSelection(adapter.getCount() - 9);
            } else {
                adapter = new DataAdapter(this, modelDataArray.getDataModels());
                listView.setAdapter(adapter);
            }

        }

        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {

            }

            @Override
            public void onScroll(AbsListView absListView, int i, int i1, int i2) {
                int total = i + i1;
//                if(adapter == null)
//                    return;
//                if(adapter.getCount() == 0)
//                    return;
                if (total == i2 && total != prev && !next_page.equals("null")) {
                    final int page = Integer.parseInt(this_page) + 1;
                    prev = total;
                    proses.getDataFromServerNext(_cari, menu, page);
                } else {
                    listView.removeFooterView(View.inflate(Produk.this,R.layout.loading, null));
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            // This ID represents the Home or Up button. In the case of this
            // activity, the Up button is shown. Use NavUtils to allow users
            // to navigate up one level in the application structure. For
            // more details, see the Navigation pattern on Android Design:
            //
            // http://developer.android.com/design/patterns/navigation.html#up-vs-back
            //
            //NavUtils.navigateUpTo(this, new Intent(this, MainActivity.class));
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
