package com.lupawaktu.halalindonesia;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.RelativeLayout;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {
    @BindView(R.id.produk)
    RelativeLayout produk;
    @BindView(R.id.produsen)
    RelativeLayout produsen;
    @BindView(R.id.sertifikat)
    RelativeLayout sertifikat;
    @BindView(R.id.aplikasi)
    RelativeLayout aplikasi;
    @BindView(R.id.admob_adview)
    AdView adview;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .addTestDevice("CC4D23ABB370FBC2813C30B6A84A4C48")// Add your real device id here
                .build();
        adview.loadAd(adRequest);


        produk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent a = new Intent(MainActivity.this, Produk.class);
                a.putExtra("menu", "produk");
                startActivity(a);
            }
        });
        produsen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent a = new Intent(MainActivity.this, Produk.class);
                a.putExtra("menu", "produsen");
                startActivity(a);
            }
        });

        sertifikat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent a = new Intent(MainActivity.this, Produk.class);
                a.putExtra("menu", "sertifikat");
                startActivity(a);
            }
        });

        aplikasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent a = new Intent(MainActivity.this, Tentang.class);
                startActivity(a);
            }
        });
    }

}
