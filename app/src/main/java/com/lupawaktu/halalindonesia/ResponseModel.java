package com.lupawaktu.halalindonesia;

/**
 * Created by Mind on 6/10/2017.
 */

public class ResponseModel {
    private String response;
    private int code;

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
