package com.lupawaktu.halalindonesia;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Mind on 6/10/2017.
 */
public class DataAdapter extends BaseAdapter {
    private Activity activity;
    private ArrayList<HashMap<String,String>> data;
    private static LayoutInflater inflater = null;

    public DataAdapter(Activity activity, ArrayList<HashMap<String,String>> data) {
        this.activity = activity;
        this.data = data;
        inflater = (LayoutInflater)  activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflater.inflate(R.layout.item_list, null);
        TextView namaproduk = (TextView) view.findViewById(R.id.namaproduk);
        TextView nomor = (TextView) view.findViewById(R.id.nomorsertifikat);
        TextView namaprodusen = (TextView) view.findViewById(R.id.namaprodusen);
        TextView berlaku = (TextView) view.findViewById(R.id.berlaku);

        namaproduk.setText(data.get(i).get("nama_produk"));
        nomor.setText("Sertifikat : "+data.get(i).get("nomor_sertifikat"));
        namaprodusen.setText("Produsen : "+data.get(i).get("nama_produsen"));
        berlaku.setText("Berlaku Hingga : "+data.get(i).get("berlaku_hingga"));
        return view;
    }
}
