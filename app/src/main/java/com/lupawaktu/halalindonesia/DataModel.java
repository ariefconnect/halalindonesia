package com.lupawaktu.halalindonesia;

/**
 * Created by Mind on 6/10/2017.
 */

public class DataModel {
    private String nama_produk;
    private String nomor_sertifikat;
    private String nama_produsen;
    private String berlaku_hingga;

    public String getNama_produk() {
        return nama_produk;
    }

    public void setNama_produk(String nama_produk) {
        this.nama_produk = nama_produk;
    }

    public String getNomor_sertifikat() {
        return nomor_sertifikat;
    }

    public void setNomor_sertifikat(String nomor_sertifikat) {
        this.nomor_sertifikat = nomor_sertifikat;
    }

    public String getNama_produsen() {
        return nama_produsen;
    }

    public void setNama_produsen(String nama_produsen) {
        this.nama_produsen = nama_produsen;
    }

    public String getBerlaku_hingga() {
        return berlaku_hingga;
    }

    public void setBerlaku_hingga(String berlaku_hingga) {
        this.berlaku_hingga = berlaku_hingga;
    }
}
