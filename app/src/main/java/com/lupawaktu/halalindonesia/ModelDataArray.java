package com.lupawaktu.halalindonesia;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Mind on 6/10/2017.
 */
public class ModelDataArray {
    private ArrayList<HashMap<String,String>> dataModels;

    public ArrayList<HashMap<String,String>> getDataModels() {
        return dataModels;
    }

    public void setDataModels(ArrayList<HashMap<String,String>> dataModels) {
        this.dataModels = dataModels;
    }
}
