package com.lupawaktu.halalindonesia;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import cz.msebera.android.httpclient.Header;

/**
 * Created by Mind on 6/10/2017.
 */

public class Proses {
    private ViewData view;
    private ResponseModel model;
    private DataModel dataModel;
    private ModelDataArray modelDataArray;

    public Proses(ViewData view) {
        this.view = view;
    }

    public void getDataFromServer(String cari, String menu, int page) {
        connectToServer(cari, menu, page);
    }

    private void connectToServer(String cari, String menu, int page) {
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("menu", menu);
        params.put("query", cari);
        params.put("page", page);
        client.get(PubClass.url, params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                success(statusCode, new String(responseBody));
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                if(statusCode != 0){
                    failure(statusCode, new String(responseBody));
                } else {
                    failure(statusCode, "Koneksi Terputus.");
                }
            }
        });
    }

    private void failure(int statusCode, String data) {
        model = new ResponseModel();
        model.setCode(statusCode);
        model.setResponse(data);
        view.failure(model);
    }

    private void success(int statusCode, String data) {
        model = new ResponseModel();
        model.setCode(statusCode);
        model.setResponse(data);
        view.success(model);
    }

    public void showConvertData(JSONArray data, ArrayList<HashMap<String,String>> dataModels) {
        dataModel = new DataModel();
        try{
            for (int a = 0; a < data.length(); a++){
                JSONObject obj = data.getJSONObject(a);
                HashMap<String,String> map = new HashMap<>();
                map.put("nama_produk", obj.getString("nama_produk"));
                map.put("nomor_sertifikat", obj.getString("nomor_sertifikat"));
                map.put("nama_produsen", obj.getString("nama_produsen"));
                map.put("berlaku_hingga", obj.getString("berlaku_hingga"));
                dataModels.add(map);
            }

            modelDataArray = new ModelDataArray();
            modelDataArray.setDataModels(dataModels);
            view.tampilData(modelDataArray);
        } catch (Exception ex){
            ex.printStackTrace();
        }
    }

    public void getDataFromServerNext(String cari, String menu, int page) {
        connectToServer(cari, menu, page);
    }

    public interface ViewData{
        void success(ResponseModel model);
        void failure(ResponseModel model);

        void tampilData(ModelDataArray modelDataArray);
    }
}
